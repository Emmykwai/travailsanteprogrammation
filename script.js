const nom = "Philippe Fleury"
const dateNaissance = new Date(1982, 12, 23)
const sexe = "H"
const situationParticuliere = false

let nbJours = prompt("Combien de jours voulez-vous analyser ?")
nbJours = parseInt(nbJours)
console.log(nbJours)

let journeesAlcool = []
for (let i = 1; i <= nbJours; i++) {
  let nbConsommationAlcool = prompt("Entrez le nombre de consommation d'alcool pour le jour " +i)
  nbConsommationAlcool = parseInt(nbConsommationAlcool)
  journeesAlcool.push(nbConsommationAlcool)
  console.log(journeesAlcool)
}

if(situationParticuliere == true){ 
    maxAlcoolsemaine = 0
    maxAlcooljour = 0
}
else if(sexe == "H"){
    maxAlcoolsemaine = 15
    maxAlcooljour = 3
}    
else { 
    maxAlcoolsemaine = 10
    maxAlcooljour = 2
}

let recommandationAlcoolRespectee = true

let today = new Date()
let aujourdhui = today.toLocaleDateString()
let heures = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
console.log(aujourdhui + " à " + heures)

console.log(nom)

function getAge(dateNaissance) { 
        let diff = Date.now() - dateNaissance.getTime()
        let age = new Date(diff)
        return Math.abs(age.getUTCFullYear() - 1970)
    }
    console.log(getAge(new Date(1982, 12, 23)) + " ans")

function obtenirMoyenne(journeesAlcool, nbJours){
    let somme = parseFloat(0)
    for(let i=0; i<journeesAlcool.length; i++){
        somme = somme + journeesAlcool[i]
    }
    return (somme/nbJours).toFixed(2)
}

function obtenirSommehebdo(journeesAlcool){
    let somme = parseFloat(0)
    for(let i=0; i<journeesAlcool.length; i++){
        somme += parseFloat(journeesAlcool[i])      
    }
    return somme
}

let moyenne = obtenirMoyenne(journeesAlcool, nbJours)
console.log("Moyenne par jour : " + moyenne)

let sommeHebdo = (obtenirSommehebdo(journeesAlcool)/nbJours)*7
console.log("Consommation sur une semaine : " + sommeHebdo)

console.log("Recommandation : " + maxAlcoolsemaine)

if(sommeHebdo>maxAlcoolsemaine){
    recommandationAlcoolRespectee = false
}

let max = Math.max(...journeesAlcool)
if(max>maxAlcooljour){
    recommandationAlcoolRespectee = false
}
console.log("Maximum en une journée : " + max)
console.log("Recommandation : " + maxAlcooljour) 

let nbJoursExcedant = journeesAlcool.filter(excedant => excedant > 2).length
let ratio = ((nbJoursExcedant/nbJours) * 100).toFixed(2)
console.log("Ratio de journées excédants : " + ratio +"%")

let nbJoursZero = journeesAlcool.filter(zero => zero == 0).length
let ratio1 = ((nbJoursZero/nbJours) * 100).toFixed(2)
console.log("Ratio de journées sans alcool : " + ratio1 + "%")

if(recommandationAlcoolRespectee == true){
    console.log("Vous respectez les recommandations")
}
else{
    console.log("Vous ne respectez pas les recommandations")
}
